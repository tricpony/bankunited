//
//  ViewController.swift
//  BankUnitedLab
//
//  Created by aarthur on 2/25/21.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    var dataSource: [Employee]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = UITableView.automaticDimension
        performService()
    }

    
    /// Fetch Restaurants.
    func performService() {
        _ = ServiceManager.sharedService.startServiceAt(urlString: API.serviceAddress) { [weak self] result in
            switch result {
            case .success(let payload):
                let fetchedObjects = JsonUtility<RootContainer>.parseJSON(payload)
                DispatchQueue.main.async {
                    self?.dataSource = fetchedObjects?.employees
                    self?.tableView.reloadData()
                }
            case .failure(let error):
                debugPrint("Service Failed: \(error.localizedDescription)")
            }
        }
    }

    // MARK: - Table View

    /// Determine the cell resuse identifier.
    /// - Parameters:
    ///   - indexPath: Index path of next cell.
    /// - Returns: Proper cell resuse identifier.
    func cellIdentifier(at indexPath: IndexPath) -> String {
        return EmployeeTableViewCell.reuseIdentifier
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource?.count ?? 0
    }

    /// Create the next cell.
    /// - Parameters:
    ///   - tableView: Destination table view.
    ///   - indexPath: Index path of next cell.
    /// - Returns: Proper cell.
    func nextCellForTableView(_ tableView: UITableView, at indexPath: IndexPath) -> EmployeeTableViewCell? {
        return tableView.dequeueReusableCell(withIdentifier: self.cellIdentifier(at: indexPath)) as? EmployeeTableViewCell
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = nextCellForTableView(tableView, at: indexPath) else { return UITableViewCell() }
        guard let employee = dataSource?[indexPath.row] else { return UITableViewCell() }
        cell.fillCell(employee)
        return cell
    }
}
