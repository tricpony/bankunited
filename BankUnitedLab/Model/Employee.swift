//
//  Employee.swift
//  BankUnitedLab
//
//  Created by aarthur on 2/25/21.
//

import Foundation

struct Employee: Codable {
    var id: String
    var name: String
    var salary: String
    var age: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case name = "employee_name"
        case salary = "employee_salary"
        case age = "employee_age"
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(String.self, forKey: .id)
        self.name = try container.decode(String.self, forKey: .name)
        self.salary = try container.decode(String.self, forKey: .salary)
        self.age = try container.decode(String.self, forKey: .age)
    }
}
