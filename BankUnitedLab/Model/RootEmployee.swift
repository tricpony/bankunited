//
//  RootContainer.swift
//  BankUnitedLab
//
//  Created by aarthur on 2/25/21.
//

import Foundation


struct RootContainer: Codable {
    var employees: [Employee]

    private enum RootKeys: String, CodingKey {
        case root = "data"
    }
    enum EmployeeKeys: String, CodingKey {
        case id
        case name = "employee_name"
        case salary = "employee_salary"
        case age = "employee_age"
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: RootKeys.self)
        self.employees = try container.decode([Employee].self, forKey: .root)
    }
}
