//
//  RootContainer.swift
//  BankUnitedLab
//
//  Created by aarthur on 2/25/21.
//

import Foundation


struct RootContainer: Codable {
    var employees: [Employee]

    private enum RootKeys: String, CodingKey {
        case root = "data"
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: RootKeys.self)
        self.employees = try container.decode([Employee].self, forKey: .root)
    }
}
