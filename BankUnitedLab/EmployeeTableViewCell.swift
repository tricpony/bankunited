//
//  EmployeeTableViewCell.swift
//  BankUnitedLab
//
//  Created by aarthur on 2/25/21.
//

import UIKit

class EmployeeTableViewCell: UITableViewCell {
    static let reuseIdentifier = "EmployeeTableViewCell"
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var salaryLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!

    func fillCell(_ employee: Employee) {
        nameLabel.text = employee.name
        salaryLabel.text = employee.salary
        ageLabel.text = employee.age
    }
}
